package useCase2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainTask3 {
	public static void main(String[] args) throws NumberFormatException, IOException {
		int number;
		boolean graj = false;
		number = (int) Math.floor(Math.random() * 11);
		System.out.println("Wpisz losow� liczbe z zakresu od 1 do 10");
		while (graj != true) {
			int userNumber;
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			userNumber = Integer.parseInt(br.readLine());
			System.out.println("bad, let's try again!");
			if (userNumber == number){
				System.out.println("Great you hit this!");
				graj = true;
			}
		}
	}

}
