package application;

import java.util.HashSet;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;

public class MoveLeftRight {

	Scene moveScene;
	static boolean reachedMax = false;
	static boolean isUpKeyPressed = false;
	HashSet<String> currentActiveKeys = new HashSet<String>();

	public MoveLeftRight(Scene moveScene) {
		this.moveScene = moveScene;
		this.addMarioEvents();
	}

	public void addMarioEvents() {

		this.moveScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent arg0) {
				currentActiveKeys.add(arg0.getCode().toString());
				AnimatedImage.isAnim = true;
				AnimatedImage ai = new AnimatedImage();
				if (currentActiveKeys.contains("LEFT")) {
					Main.marioPosX -= 5;
					AnimatedImage.moveRight = false;
				} else if (currentActiveKeys.contains("RIGHT")) {
					Main.marioPosX += 5;
					AnimatedImage.moveRight = true;
				} else if (currentActiveKeys.contains("UP") && !MoveLeftRight.isUpKeyPressed ) {
					boolean isAnimated = false;
					MoveLeftRight.isUpKeyPressed = true;
					AnimatedImage.isUp = true;
					AnimationTimer jumpAnimation = new AnimationTimer() {
						@Override
						public void handle(long now) {
							int startMarioPosY = Main.height - 96 - (int) (ai.getFrame(0).getHeight());
							if(startMarioPosY - Main.marioPosY < 150 && !MoveLeftRight.reachedMax ) {
								Main.marioPosY -= 5;
								if( startMarioPosY - Main.marioPosY == 150 ) {
									MoveLeftRight.reachedMax = true;
								}
							}

							if( MoveLeftRight.reachedMax ) {
								Main.marioPosY += 5;
								if( Main.marioPosY == startMarioPosY ) {
									stop();
									MoveLeftRight.reachedMax = false;
									MoveLeftRight.isUpKeyPressed = false;
								}
							}
						}
					};
					if( AnimatedImage.isUp ) {
						jumpAnimation.start();
						isAnimated = true;
					} else {
						jumpAnimation.stop();
						isAnimated = false;
					}
				}
				else {
					AnimatedImage.isAnim = false;
					AnimatedImage.isUp = false;
				}
			}




		});
		this.moveScene.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent arg0) {
				currentActiveKeys.remove(arg0.getCode().toString());
				AnimatedImage.isAnim = false;
				AnimatedImage.isUp = false;
			}
		});
	}

}
