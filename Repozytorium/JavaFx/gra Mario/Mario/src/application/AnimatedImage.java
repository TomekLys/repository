package application;

import javafx.scene.image.Image;

public class AnimatedImage {
	Image[] imageArray;
	Image[] imageArrayJump;
	private Image[] frames;
	private double duration;
	static boolean isAnim = false;
	static boolean isUp = false;

	static boolean moveRight = false;

	public AnimatedImage() {
		this.imageArray = new Image[4];

		this.imageArray[0] = new Image("/res/0.png");
		this.imageArray[1] = new Image("/res/1.png");
		this.imageArray[2] = new Image("/res/0R.png");
		this.imageArray[3] = new Image("/res/1R.png");

		this.imageArrayJump = new Image[6];
		this.imageArrayJump[0] = new Image("/res/2.png");
		this.imageArrayJump[1] = new Image("/res/3.png");
		this.imageArrayJump[2] = new Image("/res/4.png");
		this.imageArrayJump[3] = new Image("/res/2R.png");
		this.imageArrayJump[4] = new Image("/res/3R.png");
		this.imageArrayJump[5] = new Image("/res/4R.png");

		this.setFrames(imageArray);
	}

	public Image[] getFrames() {
		return frames;
	}

	public void setFrames(Image[] frames) {
		this.frames = frames;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public Image getFrame(double time) {
		if(AnimatedImage.isUp == false) {
			return this.getStaticFrame(time);
		}
		return this.getJumpFrame(time);
	}

	public Image getStaticFrame(double time) {
		int index = (int) ((time % (imageArray.length / 2 * duration)) / duration);

		if (AnimatedImage.isAnim == false) {
			if (AnimatedImage.moveRight == false) {
				index = 2;
			} else {
				index = 0;
			}
		} else {
			if (AnimatedImage.moveRight == false) {
				index += 2;
			}
		}
		return imageArray[index];
	}

	public Image getJumpFrame(double time) {
		int index = (int) ((time % (imageArrayJump.length / 2 * duration)) / duration);

		if (AnimatedImage.isAnim == false) {
			if (AnimatedImage.moveRight == false) {
				index = 3;
			} else {
				index = 0;
			}
		} else {
			if (AnimatedImage.moveRight == false) {
				index += 3;
			}
		}
		return imageArrayJump[index];
	}

}