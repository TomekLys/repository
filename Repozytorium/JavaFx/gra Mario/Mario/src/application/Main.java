package application;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Main extends Application {
	final static int width = 600;
	final static int height = 480;
	public static int marioPosX;
	public static int marioPosY;

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Mario");

		Group root = new Group();
		Scene theScene = new Scene(root);
		primaryStage.setScene(theScene);
		Canvas canvas = new Canvas(Main.width, Main.height);

		root.getChildren().add(canvas);

		GraphicsContext gc = canvas.getGraphicsContext2D();
		primaryStage.show();

		AnimatedImage marioMove = new AnimatedImage();


		Image backgroundImage = new Image("/res/grass.png");
		Image turtleImage = new Image("/res/zolw.png");


		marioMove.setDuration(0.1); // 0.5 sec

		MoveLeftRight move = new MoveLeftRight(theScene);


		final long startNanoTime = System.nanoTime();
		Main.marioPosX = Main.width/2 - (int) (marioMove.getFrame(0).getWidth()/2);
		Main.marioPosY = Main.height - 96 - (int)(marioMove.getFrame(0).getHeight());
		new AnimationTimer() {


			@Override
			public void handle(long now) {
				double t = (now - startNanoTime)/ Math.pow(10, 9);

				gc.clearRect(0, 0, Main.width, Main.height);
				gc.drawImage(backgroundImage, 0, 0);
				gc.drawImage(marioMove.getFrame(t), marioPosX, marioPosY);
			}
		}.start();
	}



	public static void main(String[] args) {
		launch(args);
	}
}








