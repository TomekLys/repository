package application;


import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

public class Sprite {

	private List<Image> images;
	private double durationSeconds;

	public Sprite() {
		this.images = new ArrayList<>();
		this.durationSeconds = 1.0;
	}

	public Sprite(double durationSec) {
		this.images = new ArrayList<>();
		this.durationSeconds = durationSec;
	}

	public void addImage(String pathFile) {

		this.images.add(new Image(pathFile));

	}

	public Image getFirstFrame() {
		return images.get(0);
	}

	public double getDurationSeconds() {
		return durationSeconds;
	}

	public void setDurationSeconds(double durationSeconds) {
		if (durationSeconds > 0) {
			this.durationSeconds = durationSeconds;
		} else {
			this.durationSeconds = 0.001;
		}
	}

	public Image getFrame(double time) {

		int index = (int) ((time % (this.images.size() * this.durationSeconds)) / this.durationSeconds);
	//	System.out.println(index);
	//	System.out.println(this.images.size());
		return images.get(index);
	}

}
