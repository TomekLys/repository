package application;

import javafx.geometry.Rectangle2D;

public class Player implements Collision{

	private int lifeCounter;
	private int positionX;
	private int positionY;
	private int destinationPosX;
	private int destinationPosY;
	private int veliocity;
	private boolean stopMove;
	private int width;
	private int height;

	public Player(int lifeCounter, int playerPosWidth, int playerPosHeight) {

		this.lifeCounter = lifeCounter;
		this.positionX = playerPosWidth;
		this.positionY = playerPosHeight;
		this.destinationPosX = playerPosWidth;
		this.destinationPosY = playerPosHeight;
		this.veliocity = 2;
		this.stopMove = false;
		this.width=100;
		this.height=50;

	}

	
	public int getWidth() {
		return width;
	}


	public void setWidth(int width) {
		this.width = width;
	}


	public int getHeight() {
		return height;
	}


	public void setHeight(int height) {
		this.height = height;
	}


	public void setStopMove(boolean stopMove) {
		this.stopMove = stopMove;
	}

	public int getDestinationPosWidth() {
		return destinationPosX;
	}

	public void setDestinationPosWidth(int destinationPosWidth) {
		this.destinationPosX = destinationPosWidth;
	}

	public int getDestinationPosHeight() {
		return destinationPosY;
	}

	public void setDestinationPosHeight(int destinationPosHeight) {
		this.destinationPosY = destinationPosHeight;
	}

	public int getLiveCounter() {
		return lifeCounter;
	}

	public void setLiveCounter(int liveCounter) {
		this.lifeCounter = liveCounter;
	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int posX) {
		this.positionX = posX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int posY) {
		this.positionY = posY;
	}

	public boolean CheckPlayerlife() {
		if (lifeCounter <= 0) {
			return false;
		}
		return true;
	}

	public void loseLife(int life) {
		lifeCounter -= life;
	}

	public void restoreLife(int life) {
		lifeCounter += life;
	}

	public void move() {

		if (stopMove == true) {
			destinationPosX = positionX;
			destinationPosY = positionY;
		}
		if (destinationPosX > positionX) {
			positionX += veliocity;
		} else if (destinationPosX < positionX) {
			positionX -= veliocity;
		}
		if (destinationPosY > positionY) {
			positionY += veliocity-1;
		} else if (destinationPosY < positionY) {
			positionY -= veliocity-1;
		}
	}

	@Override
	public Rectangle2D getBoundary() {
		// TODO Auto-generated method stub
		return new Rectangle2D(this.positionX, this.positionY , width, height);
	}

	@Override
	public boolean intersects(Collision s) {
		// TODO Auto-generated method stub
		return s.getBoundary().intersects(this.getBoundary());
	}

}
