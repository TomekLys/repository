
package application;

import java.net.URL;

import javafx.scene.media.AudioClip;

public class Sound {

	private static AudioClip backgroundAudio = new AudioClip( Main.class.getResource("/res/background/backgroundMusic.mp3").toExternalForm());
	private static AudioClip gameOverAudio = new AudioClip(Main.class.getResource("/res/endgame/gameOver.wav").toExternalForm());
	private static AudioClip collisonSound = new AudioClip(Main.class.getResource("/res/ship/collision.wav").toExternalForm());
	
	public void audioBackgroundPlay() {
		backgroundAudio.play();
	}

	public void audioBackgroundStop() {
		backgroundAudio.stop();
	}

	public void audioCollisionPlay() {
		collisonSound.play();
	}

	public void audioCollisionStop() {
		collisonSound.stop();
	}

	public void audioGameOver() {
		gameOverAudio.play();
	}

}
