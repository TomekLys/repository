
package pl.calendar.web.note; 

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.calendar.core.note.Note;
import pl.calendar.core.note.NoteDao;
import pl.calendar.core.user.User;
import pl.calendar.core.user.UserDao;
import pl.calendar.core.user.UserProvider;




@Controller
public class NoteController {

	@Autowired
	private NoteDao noteDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserProvider userProvider; 

	@RequestMapping(value = "/user/notes", method = RequestMethod.GET)
	public ModelAndView showNoteList() {
		ModelAndView modelAndView = new ModelAndView("note/show");
		modelAndView.addObject("notes", noteDao.getNotesByUserId(userProvider.getLoggedUser().getId()));
		return modelAndView;
	}
	
	@RequestMapping(value = "/user/note/add", method = RequestMethod.GET)
	public ModelAndView addNoteList() {
		ModelAndView modelAndView = new ModelAndView("note/add");
		modelAndView.addObject("note", new Note());
		return modelAndView;
	}

	@RequestMapping(value = "/note", method = RequestMethod.POST)
	public String addNote(@ModelAttribute Note note) {
		note.setOwner(userProvider.getLoggedUser());
		noteDao.saveOrUpdate(note);
		return "redirect:/user/notes";
	}
	
	@RequestMapping(value = "/user/note/delete", method = RequestMethod.GET)
	public String removeNote(@RequestParam("id") String id){
		Note note = noteDao.getById(Long.parseLong(id));
		noteDao.delete(note);
		return "redirect:/user/notes";
	}
	
}