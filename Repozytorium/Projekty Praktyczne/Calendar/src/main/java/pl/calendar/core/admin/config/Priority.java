package pl.calendar.core.admin.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Priority {

	@Id
	@GeneratedValue(generator = "prioritySeq")
	@SequenceGenerator(name = "prioritySeq", sequenceName = "prioritySeq")
	private Long id;
	
	@Column
	private String priorityName;
	
	@Column
	private String priorityColor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPriorityName() {
		return priorityName;
	}

	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}

	public String getPriorityColor() {
		return priorityColor;
	}

	public void setPriorityColor(String priorityColor) {
		this.priorityColor = priorityColor;
	}


	
	
}
