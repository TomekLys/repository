package pl.calendar.core.note;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.calendar.core.AbstractBaseDao;
import pl.calendar.core.user.User;

@Repository
public class NoteDaoImpl extends AbstractBaseDao<Note, Long> implements NoteDao {

	@Override
	protected Class<Note> supports() {
		return Note.class;
	}

	@Override
	public List<Note> getNotesByUserId(Long id) {
		return (List<Note>) currentSession()
				.createCriteria(Note.class)
				.add(Restrictions.eq("owner.id", id))
				.list();
	}
}