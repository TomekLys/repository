package pl.calendar.core.user;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.calendar.core.AbstractBaseDao;
import pl.calendar.core.note.Note;

@Repository
public class UserDaoImpl extends AbstractBaseDao<User, Long> implements UserDao {

	@Override
	protected Class<User> supports() {
		return User.class;
	}

	public User getByLogin(String login) {
		return (User) currentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("email", login))
				.uniqueResult();
	}



}
