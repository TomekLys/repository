package pl.calendar.core.user;

public enum UserRole {
	
	ROLE_USER,
	ROLE_ADMIN;

}
