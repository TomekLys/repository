package pl.calendar.web.register;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import pl.calendar.core.user.User;
import pl.calendar.core.user.UserDao;
import pl.calendar.core.user.UserRole;

@Controller
public class RegisterController {

	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value ="/admin/users", method = RequestMethod.GET)
	public ModelAndView showUserList(){
		ModelAndView modelAndView = new ModelAndView("admin/showUsers");
		modelAndView.addObject("users", userDao.getAll());
		return modelAndView;
	}
	
	
	@RequestMapping(value = "/register/add", method = RequestMethod.GET)
	public ModelAndView addUserList() {
		ModelAndView modelAndView = new ModelAndView("register");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView addUser(@Valid @ModelAttribute User user, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			ModelAndView modelAndView = new ModelAndView("register");
			return modelAndView;
		}
		user.setRole(UserRole.ROLE_ADMIN);
		userDao.saveOrUpdate(user);
		return new ModelAndView("login");
	}
	
	@RequestMapping(value = "/user/delete", method = RequestMethod.GET)
	public String removeUser(@RequestParam("id") String id){
		User user = userDao.getById(Long.parseLong(id));
		userDao.delete(user);
		return "redirect:/admin/users";
	}
	
}
