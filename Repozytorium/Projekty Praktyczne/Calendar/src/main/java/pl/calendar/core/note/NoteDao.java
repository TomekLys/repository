package pl.calendar.core.note;

import java.util.List;

import pl.calendar.core.BaseDao;
import pl.calendar.core.note.Note;


public interface NoteDao extends BaseDao<Note, Long>{
	public List<Note> getNotesByUserId(Long id);
}
