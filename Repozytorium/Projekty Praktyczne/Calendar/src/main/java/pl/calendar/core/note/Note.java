package pl.calendar.core.note;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.springframework.format.annotation.DateTimeFormat;

import pl.calendar.core.user.User;

@Entity
public class Note {
	
	@Id
	@GeneratedValue(generator = "noteSeq")
	@SequenceGenerator(name = "noteSeq", sequenceName = "noteSeq")
	private Long id;
	
	@Column
	private String name;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
	@Column
	private Date dataStart;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
	@Column
	private Date dataEnd;	

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User owner;
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}



	public Date getDataEnd() {
		return dataEnd;
	}

	public void setDataEnd(Date dataEnd) {
		this.dataEnd = dataEnd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Date getDataStart() {
		return dataStart;
	}

	public void setDataStart(Date dataStart) {
		this.dataStart = dataStart;
	}

	public void setName(String name) {
		this.name = name;
	}
}
