package pl.calendar.core.admin.config;

import org.springframework.stereotype.Repository;

import pl.calendar.core.AbstractBaseDao;
import pl.calendar.core.note.Note;
import pl.calendar.core.note.NoteDao;

@Repository
public class PriorityDaoImpl extends AbstractBaseDao<Priority, Long> implements PriorityDao {

	@Override
	protected Class<Priority> supports() {

		return Priority.class;

	}

}
