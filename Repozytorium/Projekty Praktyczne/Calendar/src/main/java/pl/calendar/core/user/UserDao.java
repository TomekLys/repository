package pl.calendar.core.user;

import java.util.List;

import pl.calendar.core.BaseDao;
import pl.calendar.core.note.Note;

public interface UserDao extends BaseDao<User, Long> {
	
	User getByLogin(String login);
}
