package pl.calendar.web.admin.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.calendar.core.admin.config.Priority;
import pl.calendar.core.admin.config.PriorityDao;
import pl.calendar.core.user.User;


@Controller
public class AdminConfigController {


	@Autowired 
	private PriorityDao priorityDao;
	
	@RequestMapping(value = "/admin/settings", method = RequestMethod.GET)
	public ModelAndView showSettings() {
		ModelAndView modelAndView = new ModelAndView("admin/settings");
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/settings/priority/show", method = RequestMethod.GET)
	public ModelAndView showPriorityList() {
		ModelAndView modelAndView = new ModelAndView("admin/priorityShow");
		modelAndView.addObject("priority", priorityDao.getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/setting/priority/add", method = RequestMethod.GET)
	public ModelAndView showPrioritetSettings() {
		ModelAndView modelAndView = new ModelAndView("admin/priorityAdd");
		modelAndView.addObject("priority", new Priority());
		return modelAndView;
	}
	
	@RequestMapping(value = "/admin/setting/add/priority", method = RequestMethod.POST)
	public String addPrioritySetting(@ModelAttribute Priority priority) {

		priorityDao.saveOrUpdate(priority);
		return "redirect:/admin/settings/priority/show";
	}
	
	@RequestMapping(value = "/admin/setting/delete/prioritet", method = RequestMethod.GET)
	public String removePrioritySetting(@RequestParam("id") String id) {
		Priority priority = priorityDao.getById(Long.parseLong(id));
		priorityDao.delete(priority);
		return "redirect:/admin/settings/priority/show";
	}
	
	

}
