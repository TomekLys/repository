<%@ page session="false" isELIgnored="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<body>
	<spring:url value="/register" var="addUserAction" />

	<h2>Register</h2>


	<form:form method="post" modelAttribute="user"
		action="${addUserAction}">
		<table>
			<tr>
				<td><label>Pseudonim</label></td>
				<td><spring:bind path="fullName">
						<form:input path="fullName" id="fullName" />
						<form:errors path="fullName" />
					</spring:bind></td>
			</tr>
			<tr>
				<td><label>Email (login)</label></td>
				<td><spring:bind path="email">

						<form:input path="email" id="email" />
						<form:errors path="email" />

					</spring:bind></td>
			</tr>
			<tr>
				<td><label>Haslo</label></td>
				<td><spring:bind path="password">

						<form:input path="password" id="password" />
						<form:errors path="password" />

					</spring:bind></td>
			</tr>
			<tr>
				<td>
					<button type="submit">Add</button>
				</td>
			</tr>
		</table>
	</form:form>
</body>
</html>