<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html lang="en">
<spring:url value="/js/" var="js" />
<spring:url value="/css/" var="css" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="${css}jquery.datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" href="${css}bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${css}style.css" />
<body>
	<c:url value="/" var="homeUrl" />
	<c:url value="/logout" var="logoutUrl" />
	<c:url value="/user/notes" var="noteUrl" />
	<c:url value="/user/note/add" var="addNoteUrl" />
	<c:url value="admin/users" var="usersUrl" />
	<c:url value="/admin/settings" var="settingsUrl" />




	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Kalendarz</a>
			</div>

			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a href="${homeUrl}">Home <span
							class="sr-only">(current)</span></a></li>
					<li><a href="${usersUrl}">Uzytkownicy</a></li>

					<li><a href="${noteUrl}">Notatnik</a></li>
					<li><a href="${addNoteUrl}"> + Nowa Notatka</a></li>
				</ul>
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control some_class" id="datetimepicker2"
							placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="${settingsUrl}">Settings</a></li>
					<li><a href="${logoutUrl}">Logout</a></li>
			
				</ul>
			</div>
		</div>
	</nav>


	<h2>Witaj</h2>





	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="${js}bootstrap.min.js"></script>
	<script src="${js}jquery.js"></script>
	<script src="${js}jquery.datetimepicker.full.min.js"></script>
	<script>
	$('#datetimepicker2').datetimepicker({
		lang:'ch',
		timepicker:false,
		format:'Y/m/d',
		formatDate:'Y/m/d',
		minDate:'-2015/01/02', // yesterday is minimum date
		maxDate:'+2050/01/02' // and tommorow is maximum date calendar
	});
	</script>
</body>
</html>