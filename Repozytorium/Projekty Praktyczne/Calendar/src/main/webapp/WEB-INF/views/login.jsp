<%@ page session="false" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">



<body>
	<c:url value="/register/add" var="registerUrl" />
	<div>
<div class="row">
  <div class="col-xs-6 col-md-4"></div>
  	  <div class="col-xs-6 col-md-4">
 
			<h3>Zaloguj się </h3>
	
			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="msg">${msg}</div>
			</c:if>
	
				<form name='loginForm' action="<c:url value='/login' />" method='POST'>
		
					<table>
						<tr>
							<td>Email:</td>
							<td><input type='text' name='username'></td>
						</tr>
						<tr>
							<td>Password:</td>
							<td><input type='password' name='password' /></td>
						</tr>
						<tr>
							<td colspan='2'><input name="submit" type="submit"
								value="submit" /><a href="${registerUrl}">Rejestracja</a>
						</tr>
					</table>
		
				</form>
		</div>
	</div>
 <div class="col-xs-6 col-md-4"></div>
</div>	
	

</body>


</html>