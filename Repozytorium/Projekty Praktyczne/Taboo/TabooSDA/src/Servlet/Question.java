package Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Controller.TabooController;
import Model.TabooModel;

@WebServlet("/Question")
public class Question extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Question() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		HttpSession ses = request.getSession();

		if (action.equalsIgnoreCase("AddQuest")) {

			String wordGuess = request.getParameter("WordToGuess");
			String tabooWord1 = request.getParameter("TabooWord1");
			String tabooWord2 = request.getParameter("TabooWord2");
			String tabooWord3 = request.getParameter("TabooWord3");
			String tabooWord4 = request.getParameter("TabooWord4");
			String tabooWord5 = request.getParameter("TabooWord5");

			TabooModel tabooM = new TabooModel(0, wordGuess, tabooWord1, tabooWord2, tabooWord3, tabooWord4,
					tabooWord5);
			TabooController tc = new TabooController();
			tc.addNewQuestionToDB(tabooM);

			response.sendRedirect("index.jsp");
		}
	}
}
