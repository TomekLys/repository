package Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="tabooQuestions")
public class TabooModel {
	
	@Id
	private int id;
	private String wordToGuess;
	private String firstWord;
	private String secondWord;
	private String thirdWord;
	private String fourthWord;
	private String fifthWord;
	
	public TabooModel(){}

	public TabooModel(int id, String wordToGuess, String firstWord,
			String secondWord, String thirdWord, String fourthWord,
			String fifthWord) {
		
		this.id = id;
		this.wordToGuess = wordToGuess;
		this.firstWord = firstWord;
		this.secondWord = secondWord;
		this.thirdWord = thirdWord;
		this.fourthWord = fourthWord;
		this.fifthWord = fifthWord;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWordToGuess() {
		return wordToGuess;
	}

	public void setWordToGuess(String wordToGuess) {
		this.wordToGuess = wordToGuess;
	}

	public String getFirstWord() {
		return firstWord;
	}

	public void setFirstWord(String firstWord) {
		this.firstWord = firstWord;
	}

	public String getSecondWord() {
		return secondWord;
	}

	public void setSecondWord(String secondWord) {
		this.secondWord = secondWord;
	}

	public String getThirdWord() {
		return thirdWord;
	}

	public void setThirdWord(String thirdWord) {
		this.thirdWord = thirdWord;
	}

	public String getFourthWord() {
		return fourthWord;
	}

	public void setFourthWord(String fourthWord) {
		this.fourthWord = fourthWord;
	}

	public String getFifthWord() {
		return fifthWord;
	}

	public void setFifthWord(String fifthWord) {
		this.fifthWord = fifthWord;
	};
	
}
