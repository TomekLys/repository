package Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "statistic")
public class StatisticModel {

	@Id
	private int teamId;
	private int score;
	private int trueAnswers;
	private int falseAnswers;

	public StatisticModel() {

	}

	public StatisticModel(int teamId, int score, int trueAnswers, int falseAnswers) {

		this.teamId = teamId;
		this.score = score;
		this.trueAnswers = trueAnswers;
		this.falseAnswers = falseAnswers;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getTrueAnswers() {
		return trueAnswers;
	}

	public void setTrueAnswers(int trueAnswers) {
		this.trueAnswers = trueAnswers;
	}

	public int getFalseAnswers() {
		return falseAnswers;
	}

	public void setFalseAnswers(int falseAnswers) {
		this.falseAnswers = falseAnswers;
	}

}
