package Controller;

import org.hibernate.Session;
import org.hibernate.Transaction;

import Controller.AppController;
import Model.StatisticModel;
import Model.TabooModel;

public class StatisticController {

	private StatisticModel teamId;
	private StatisticModel score; 
	private StatisticModel trueAnswers;
	private StatisticModel falseAnswers;
	
	public StatisticController() {

	}

	public StatisticController(StatisticModel teamId, StatisticModel score, StatisticModel trueAnswers, StatisticModel falseAnswers) {

		this.teamId = teamId;
		this.score = score;
		this.trueAnswers = trueAnswers;
		this.falseAnswers = falseAnswers;
	}

	public StatisticModel getTeamId() {
		return teamId;
	}

	public void setTeamId(StatisticModel teamId) {
		this.teamId = teamId;
	}

	public StatisticModel getScore() {
		return score;
	}

	public void setScore(StatisticModel score) {
		this.score = score;
	}

	public StatisticModel getTrueAnswers() {
		return trueAnswers;
	}

	public void setTrueAnswers(StatisticModel trueAnswers) {
		this.trueAnswers = trueAnswers;
	}

	public StatisticModel getFalseAnswers() {
		return falseAnswers;
	}

	public void setFalseAnswers(StatisticModel falseAnswers) {
		this.falseAnswers = falseAnswers;
	}

	public void insertStatistic(int teamId, int score, int trueAnswers, int falseAnswers){
		try{
			Session s = AppController.getSession().openSession();
			Transaction t = s.beginTransaction();
			
			StatisticModel tm = new StatisticModel(teamId, score, trueAnswers, falseAnswers);
			
			s.save(tm);
			t.commit();
			s.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	
}
