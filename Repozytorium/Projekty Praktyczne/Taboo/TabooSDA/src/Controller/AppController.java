package Controller;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AppController {

	public static SessionFactory sf;
	public static boolean newDB = true;
	public static SessionFactory getSession() {
		return sf;
	}
	
	public static void setSession(){
		try {
			sf = new Configuration().configure().buildSessionFactory();
			newDB=false;
			} catch (Exception e) {
			System.out.println(e.getMessage());
			}
	}
}