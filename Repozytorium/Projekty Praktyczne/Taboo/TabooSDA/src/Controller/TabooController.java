package Controller;

import java.util.List;

import org.hibernate.Session;

import org.hibernate.Transaction;

import Model.*;

public class TabooController {

	private TabooModel tm;

	public TabooController() {
		super();
	}

	public TabooController(int id, String wordToGuess, String firstWord, String secondWord, String thirdWord,
			String fourthWord, String fifthWord) {
		super();

		this.tm = new TabooModel(id, wordToGuess, firstWord, secondWord, thirdWord, fourthWord, fifthWord);

	}

	public int getId() {
		return tm.getId();
	}

	public void setId(int id) {
		this.tm.setId(id);
	}

	public String getWordToGuess() {
		return tm.getWordToGuess();
	}

	public void setWordToGuess(String wordToGuess) {
		this.tm.setWordToGuess(wordToGuess);
	}

	public String getFirstWord() {
		return tm.getFifthWord();
	}

	public void setFirstWord(String firstWord) {
		this.tm.setFirstWord(firstWord);
	}

	public String getSecondWord() {
		return tm.getSecondWord();
	}

	public void setSecondWord(String secondWord) {
		this.tm.setSecondWord(secondWord);
	}

	public String getThirdWord() {
		return tm.getThirdWord();
	}

	public void setThirdWord(String thirdWord) {
		this.tm.setThirdWord(thirdWord);
	}

	public String getFourthWord() {
		return tm.getFourthWord();
	}

	public void setFourthWord(String fourthWord) {
		this.tm.setFourthWord(fourthWord);
	}

	public String getFifthWord() {
		return tm.getFifthWord();
	}

	public void setFifthWord(String fifthWord) {
		this.tm.setFifthWord(fifthWord);
	}

	public boolean addNewQuestionToDB(TabooModel newQuestion) {
		try {
			Session ses = AppController.sf.openSession();
			Transaction trans = ses.getTransaction();

			ses.save(newQuestion);
			trans.commit();
			ses.close();

		} catch (Exception e) {
			System.out.println();
			return false;
		}

		return true;
	}

	public boolean addNewQuestionToDB() {
		try {
			Session ses = AppController.sf.openSession();
			Transaction trans = ses.getTransaction();

			ses.save(this.tm);
			trans.commit();
			ses.close();

		} catch (Exception e) {
			System.out.println();
			return false;
		}

		return true;
	}

	public boolean removeQuestionFromDB(TabooModel deleteQuestion) {
		try {
			Session ses = AppController.sf.openSession();
			Transaction trans = ses.getTransaction();

			ses.delete(deleteQuestion);
			trans.commit();
			ses.close();

		} catch (Exception e) {
			System.out.println();
			return false;
		}

		return true;
	}

	public boolean removeQuestionFromDB() {
		try {
			Session ses = AppController.sf.openSession();
			Transaction trans = ses.getTransaction();

			ses.delete(this.tm);
			trans.commit();
			ses.close();

		} catch (Exception e) {
			System.out.println();
			return false;
		}

		return true;
	}

	public boolean updateQuestionInDB(TabooModel updateQuestion) {
		try {
			Session ses = AppController.sf.openSession();
			Transaction trans = ses.getTransaction();

			ses.update(updateQuestion);
			trans.commit();
			ses.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}

	public boolean updateQuestionInDB() {
		try {
			Session ses = AppController.sf.openSession();
			Transaction trans = ses.getTransaction();

			ses.update(this.tm);
			trans.commit();
			ses.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}

	public TabooModel getQuestionFromDB(int id) {
		try {
			Session ses = AppController.sf.openSession();

			TabooModel tModel = (TabooModel) ses.createQuery("FROM TabooModel WHERE id=:id").setParameter("id", id);

			ses.close();
			return tModel;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	public List<TabooModel> getAllQuestionsFromDB() {

		List<TabooModel> records = null;
		try {
			Session ses = AppController.sf.openSession();

			records = ses.createQuery("FROM TabooModel").list();

			ses.close();
			return records;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
}
