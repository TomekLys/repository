import java.awt.*;
import java.awt.event.*;

class ButtonDemo1 extends Frame {

    Button b1;
    Button b2;
    Button switchbutton;
    TextField t1;
    

    public ButtonDemo1() {
        setTitle("Hi! I am a Frame");
        setSize(400, 400);
        setLayout(new FlowLayout());
        setVisible(true);
        repaint();
        // Just add a button (optional)
        b1 = new Button("OK");
        b2 = new Button("Close");
        t1 = new TextField();
        switchbutton = new Button("Zmien Ten przycisk");
        
        add(b1);
        add(b2);
        add(t1);
        add(switchbutton);
        
        b1.addActionListener(new WritesReverseNumber());
        b2.addActionListener(new CloseApp());
        switchbutton.addActionListener(new Renamebutton());
       
        
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                

            	System.exit(0);
            }
        });
    }

    public static void main(String args[]) {
        new ButtonDemo1();
    }
}
