public class Person implements Comparable<Person> {
	private String name;
	private String surname;
	private String company;
	
	
	public Person(String name, String surname, String company) {
		super();
		this.name = name;
		this.surname = surname;
		this.company = company;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getCompany() {
		return company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	@Override
	public String toString() {
		return "Person [name=" + name + ", surname=" + surname + ", company=" + company + "]";
	}


	@Override
	public int compareTo(Person o) {
		
		return this.surname.compareTo(o.surname);
	}
	
	
	
	
	
}