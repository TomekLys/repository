import org.junit.Assert;
import org.junit.Test;



public class Point2dTest {

	@Test
		public void shouldReturnTrueForTwoEqualPoints() {
			//given
		Point2d p1 = new Point2d(1, 8);
		Point2d p2 = new Point2d(1, 8);
			//when
			boolean result = p1.equals(p2);
			
			//then 
			Assert.assertEquals(true, result);
		}
	

	@Test
		public void shouldReturnFalseForTwoEqualPoints() {
			//given
		Point2d p1 = new Point2d(1, 8);
		Point2d p2 = new Point2d(1, 8);
			//when
			boolean result = p1.equals(p2);
			
			//then 
			Assert.assertEquals(false, result);
		}
		
		
	}

