
public enum Color {

	Red(255,0,0),
	Green(0,255,0),
	Yellow(255,255,0);
	
	private int R;
	private int G;
	private int B;
	
	Color(int R, int G, int B) {
		this.R = R;
		this.G = G;
		this.B = B;
	}
	@Override
	public String toString(){
		return "Moje kolory to " + R + ", " + G + ", "+ B;
	}
	
}
