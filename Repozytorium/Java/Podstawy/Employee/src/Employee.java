
public class Employee implements Comparable<Employee> {

	private String name;
	private String surname;
	private int age;
	private int seniority;
	
	public Employee(String name, String surname,int age, int seniority) {

		this.name = name;
		this.surname = surname;
		this.age = age;
		this.seniority = seniority;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getOld() {
		return age;
	}

	public void setOld(int age) {
		this.age = age;
	}

	public int getSeniority() {
		return seniority;
	}

	public void setSeniority(int seniority) {
		this.seniority = seniority;
	}
	
	@Override
	public int compareTo(Employee other) {
	    int i = name.compareTo(other.name);
	    if (i != 0) return i;

	    i = surname.compareTo(other.surname);
	    if (i != 0) return i;

	    return Integer.compare(age, other.age);
	}

}




