import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class FileExist {

	public static void main(String[] args) {
		Set<String> wordSet = new HashSet<>();
		wordSet.add("Witam serdecznie");
		wordSet.add("Hejo");
		try {
			writeSetToFile(wordSet, "filee.txt");
		} catch (WrongPathException e) {
		
		}

	}
	
	@SuppressWarnings("resource")
	public static void writeSetToFile(Set<String> wordSet, String filePath) throws WrongPathException{
		Set<String> treeSet = convertSetToTreeSet(wordSet);
		try{
			 File file = new File(filePath); 
			 BufferedWriter bufferedWriter =  new BufferedWriter(new FileWriter(file));
			 for(String line : treeSet){
				 bufferedWriter.write(line);
				 bufferedWriter.newLine();
			 }
			 
			 bufferedWriter.flush();
	            
	      }catch(IOException e){
	         e.printStackTrace();
	      }
	}
	
	public static TreeSet<String> convertSetToTreeSet(Set<String> wordSet){
		Set<String> treeSet = new TreeSet<String>();
		
		for(String line : wordSet){
			 treeSet.add(line);
		 }
		return (TreeSet<String>) treeSet;
	}
}

