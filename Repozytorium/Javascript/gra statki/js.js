var translate = []
    translate["A"] = 0;
    translate["B"] = 1;
    translate["C"] = 2;
    translate["D"] = 3;
    translate["E"] = 4;
    translate["F"] = 5;
    translate["G"] = 6;
    translate["H"] = 7;
	translate["a"] = 0;
    translate["b"] = 1;
    translate["c"] = 2;
    translate["d"] = 3;
    translate["e"] = 4;
    translate["f"] = 5;
    translate["g"] = 6;
    translate["h"] = 7;

var trans = ["A", "B", "C", "D", "E", "F", "G", "H"];

function createBoard(dim, ships) {
    var table = [];
    var x;
    var y;
    var proby = 0;
    
    for(var i = 0; i < dim; i++) {
        table[i] = [];
        for(var j = 0; j < dim; j++) {
            table[i][j] = 0;
        }
    }
    
    while (ships > 0 && proby < 100) {
        x = Math.floor(Math.random() * dim);
        y = Math.floor(Math.random() * dim);

        if (canBePlaced(table, x, y)) {
            table[x][y] = 1;  
            ships--;
            proby = 0;
        } else {
            proby++;
        }
    }
    
    if (ships > 0) {
        alert('Blad!');
        return false;
    } else {
        return table;
    }
}

function canBePlaced(table, x, y) {
    if (table[x][y] !== 1 &&
        (typeof(table[x-1]) === 'undefined' ||
            (table[x-1][y-1] !== 1 &&
            table[x-1][y] !== 1 &&
            table[x-1][y+1] !== 1)) &&
        (typeof(table[x+1]) === 'undefined' ||
            (table[x+1][y-1] !== 1 &&
            table[x+1][y] !== 1 &&
            table[x+1][y+1] !== 1)) &&        
        table[x][y-1] !== 1 &&
        table[x][y+1] !== 1 
        ) {
        return true;
    } else {
        return false;
    }
}

function getX() {
    var x;
    do {
        x = prompt("Podaj wartosc X (zakres od A do H)");
    	if(x === "-1"){
			return false;
		}
	} while (typeof(translate[x]) === 'undefined')
            
    return translate[x];
}

function getY() {
    var y;
    do {
        y = prompt("Podaj wartosc Y (zakres od 1 do 7) lub -1 aby wyłączyc program");
		y = parseInt(y);
		
		if(y === "-1"){
			return false;
		}
    } while (isNaN(y) && y > 0)
        
    return y - 1;
}

function game(board, ships) {
    var history = [];
    
    while(ships > 0) {
        var x = getX();
        var y = getY();
		
		if (x === false) {
			return;
		}
		if (y === false) {
			return;
		}

        if (board[x][y] === 0) {
            board[x][y] = 3;
            alert("Pudło!");
            history.push([x,y,0]);
        } else if (board[x][y] === 1) {
            board[x][y] = 2;
            alert("Trafiony!");
            ships--;
            history.push([x,y,1]);
        } 
        writeBoard(board);
    } 
    console.log(history);
    alert(coverage(board.length, history.length))
}

function coverage(dim, shots) {
    return Math.round((shots / Math.pow(dim, 2)) * 10000) / 100;
}

function formatTime(time) {
    var gameTime = Math.floor(time / 1000);
    var gameMin = Math.floor(gameTime / 60);
    var gameSec = gameTime - gameMin * 60;
    gameSec = gameSec < 10 ? "0" + gameSec : gameSec;
    gameMin = gameMin < 10 ? "0" + gameMin : gameMin;
    
    alert(gameMin + ":" + gameSec);
}

function writeBoard(board) {
    document.write("X: ");
    for(var z = 1; z <= board.length; z++) {
        document.write(z + " ");
    }
    document.write("<br/>");
    
    for(var i = 0; i < board.length; i++) {
        document.write(trans[i] + ": ");
        for(var j = 0; j < board[i].length; j++) {
            switch (board[i][j]) {
                case 1: document.write(0); break;    
                case 2: document.write("Z"); break;
                case 3: document.write("X"); break;
                default: document.write(board[i][j]);
            }
            
            document.write(" ");
        }
        document.write("<br/>");
    }
    document.write("<br/>");
}

function zwykla() {
    var start = new Date();
    game(board, ships);

    var end = new Date();
    formatTime(end.getTime() - start.getTime()); 
}

var ships = 10;
alert("liczba statkow " + window.ships);
var board = createBoard(7,ships);

if (board !== false) {
    setTimeout(zwykla, 500);
}
